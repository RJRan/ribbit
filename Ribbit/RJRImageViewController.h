//
//  RJRImageViewController.h
//  Ribbit
//
//  Created by Robert Randell on 02/06/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface RJRImageViewController : UIViewController

@property (nonatomic) PFObject *message;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
