//
//  RJRSignupViewController.h
//  Ribbit
//
//  Created by Robert Randell on 02/05/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RJRSignupViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *emailAddressField;

- (IBAction)signUp:(id)sender;

@end
