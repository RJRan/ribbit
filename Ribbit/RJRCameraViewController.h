//
//  RJRCameraViewController.h
//  Ribbit
//
//  Created by Robert Randell on 30/05/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface RJRCameraViewController : UITableViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic) UIImagePickerController *imagePicker;
@property (nonatomic) UIImage *image;
@property (nonatomic) NSString *videoFilePath;
@property (nonatomic) NSArray *friends;
@property (nonatomic) NSMutableArray *recipients;

@property (nonatomic) PFRelation *friendsRelation;

- (IBAction)cancel:(id)sender;
- (IBAction)send:(id)sender;

- (void)uploadMessage;
- (UIImage *)resizeImage:(UIImage *)image toWidth:(float)width andHeight:(float)height;

@end
