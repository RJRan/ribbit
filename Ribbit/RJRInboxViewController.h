//
//  RJRInboxViewController.h
//  Ribbit
//
//  Created by Robert Randell on 02/05/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <MediaPlayer/MediaPlayer.h>

@interface RJRInboxViewController : UITableViewController

@property (nonatomic) NSArray *messages;
@property (nonatomic) PFObject *selectedMessage;
@property (nonatomic) MPMoviePlayerController *moviePlayer;

- (IBAction)logOut:(id)sender;

@end
