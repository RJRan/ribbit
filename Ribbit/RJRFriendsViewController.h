//
//  RJRFriendsViewController.h
//  Ribbit
//
//  Created by Robert Randell on 30/05/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface RJRFriendsViewController : UITableViewController

@property (nonatomic) PFRelation *friendsRelation;
@property (nonatomic) NSArray *friends;

@end
