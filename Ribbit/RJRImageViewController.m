//
//  RJRImageViewController.m
//  Ribbit
//
//  Created by Robert Randell on 02/06/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRImageViewController.h"

@interface RJRImageViewController ()

@end

@implementation RJRImageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    PFFile *imageFile = [self.message objectForKey:@"file"];
    NSData *imageData = [imageFile getData];
    //NSURL *imageFileURL = [[NSURL alloc] initWithString:imageFile.url];
    //NSData *imageData = [NSData dataWithContentsOfURL:imageFileURL];
    self.imageView.image = [UIImage imageWithData:imageData];
    
    NSString *senderName = [self.message objectForKey:@"senderName"];
    NSString *title = [NSString stringWithFormat:@"Sent from %@", senderName];
    self.navigationItem.title = title;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([self respondsToSelector:@selector(timeOut)]) {
        [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(timeOut) userInfo:nil repeats:NO];
    }
    else {
        NSLog(@"Error: Selector Missing!");
    }
}

#pragma mark - Helper methods

- (void)timeOut {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
