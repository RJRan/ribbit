//
//  main.m
//  Ribbit
//
//  Created by Robert Randell on 02/05/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RJRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RJRAppDelegate class]));
    }
}
