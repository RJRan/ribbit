//
//  RJREditFriendsViewController.h
//  Ribbit
//
//  Created by Robert Randell on 30/05/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface RJREditFriendsViewController : UITableViewController

@property (nonatomic) NSArray *allUsers;
@property (nonatomic) NSMutableArray *friends;
@property (nonatomic) PFUser *currentUser;

- (BOOL)isFriend:(PFUser *)user;

@end
